import json
import random
from dataclasses import dataclass
from decimal import Decimal
from typing import Dict, List

"""
`distribution.py`
1) Reads and validates the user group percentages json file. 
2) Assigns a group based on an input.
3) 
"""


class DistributionValueError(Exception):
    pass


class DistributionValueSumError(Exception):
    pass


@dataclass
class GroupDistribution:
    floor: Decimal
    ceiling: Decimal
    group_name: str


def read_json_distribution(file_path: str) -> List[GroupDistribution]:
    with open(file_path) as f:
        data = json.load(f, parse_float=Decimal)

        validated_data = validate_and_transform_distribution(data)
        return validated_data


def validate_and_transform_distribution(distribution: Dict[str, Decimal]) -> List[GroupDistribution]:
    """
    Validates and transforms the input distribution data. Raises exceptions if it is not the correct format.

    :param distribution: Input dictionary with group names and percentages.
    :return: List of validated GroupDistribution
    """
    validated_distributions: List[GroupDistribution] = []
    current_floor = Decimal(0)
    for k, v in distribution.items():
        # Key validation occurs upstream in the read_json_distribution. Only check the type of the value.
        if type(v) is not Decimal:
            raise DistributionValueError(f"Group percentage is not a decimal. Found: {v}")
        current_ceiling = current_floor + v
        validated_distributions.append(GroupDistribution(floor=current_floor, ceiling=current_floor + v, group_name=k))
        current_floor = current_ceiling

    if current_floor != 1.0:
        raise DistributionValueSumError(f"Group percentages did not add up to 1.0. Found: {current_floor}")

    return validated_distributions


def assign_group(input_point: Decimal, group_distributions: List[GroupDistribution]) -> str:
    for gd in group_distributions:
        # Since this is a List, only floor is really needed here as there is ordering. Ceiling is used as an additional sanity check.
        if gd.floor <= input_point < gd.ceiling:
            return gd.group_name
    raise ValueError(f"Could not find a group range. Is the value {input_point} out of the range [0,1)?")


def get_user_group(user_id: str, group_distributions: List[GroupDistribution], user_group_cache: Dict[str, str]) -> str:
    assigned_group = user_group_cache.get(user_id)
    if assigned_group:
        return assigned_group
    else:
        rd = Decimal(random.random())
        assigned_group = assign_group(rd, group_distributions)
        # Update the cache via side-effect. This is intended to run as a single threaded process.
        user_group_cache[user_id] = assigned_group
        return assigned_group
