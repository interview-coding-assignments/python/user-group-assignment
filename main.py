import os

from distribution import get_user_group, read_json_distribution

if __name__ == '__main__':
    file_path = os.path.join(os.path.dirname(__file__))
    group_percentages_json = os.path.join(file_path, "resources", "userGroupPercentages.json")
    group_distribution_list = read_json_distribution(group_percentages_json)

    cache = {}
    users = [
        "user1",
        "user2",
        "user3",
        "user4",
        "user5",
        "user6",
        "user7",
        "user8",
        "user9",
        "user10",
    ]

    # NOTE: This is meant to be single threaded and sequential. A simple dictionary is used as the cache.
    for u in users:
        group = get_user_group(u, group_distribution_list, cache)
        print(f"get_user_group('{u}') --> {group}")

    print("\nLet's print out a cached user_id 5 times.")
    # Print a cached user_id
    for _ in range(5):
        group = get_user_group(users[8], group_distribution_list, cache)
        print(f"get_user_group('{users[8]}') --> {group}")

