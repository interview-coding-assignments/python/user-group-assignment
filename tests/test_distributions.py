import os
import unittest
from json import JSONDecodeError

from distribution import *


class TestDistributions(unittest.TestCase):
    test_dir_path = os.path.join(os.path.dirname(__file__), "resources")
    users = [
        "1",
        "2",
        "3",
        "4",
        "5",
        "6",
        "7",
        "8",
        "9",
        "10"
    ]

    def test_read_json_distribution_with_valid_data(self):
        distribution_path = os.path.join(self.test_dir_path, "testValidUserGroupPercentages.json")
        actual_group_distributions = read_json_distribution(distribution_path)
        expected_group_distributions = [
            GroupDistribution(floor=Decimal('0'), ceiling=Decimal('0.4'), group_name='groupA'),
            GroupDistribution(floor=Decimal('0.4'), ceiling=Decimal('0.5'), group_name='groupB'),
            GroupDistribution(floor=Decimal('0.5'), ceiling=Decimal('0.8'), group_name='groupC'),
            GroupDistribution(floor=Decimal('0.8'), ceiling=Decimal('0.9'), group_name='groupD'),
            GroupDistribution(floor=Decimal('0.9'), ceiling=Decimal('1.0'), group_name='groupE')
        ]

        self.assertEqual(actual_group_distributions, expected_group_distributions)

    def test_read_json_distribution_with_invalid_json_key(self):
        # This file has a number as a key.
        distribution_path = os.path.join(self.test_dir_path, "testInvalidJsonKey.json")
        with self.assertRaises(JSONDecodeError):
            read_json_distribution(distribution_path)

    def test_read_json_distribution_with_invalid_json(self):
        # This file is broken json.
        distribution_path = os.path.join(self.test_dir_path, "testInvalidJson.json")
        with self.assertRaises(JSONDecodeError):
            read_json_distribution(distribution_path)

    def test_validate_and_transform_distribution_with_valid_data(self):
        input_dict = {
            "groupA": Decimal(0.5),
            "groupB": Decimal(0.5)
        }
        actual_group_distributions = validate_and_transform_distribution(input_dict)
        expected_group_distributions = [
            GroupDistribution(floor=Decimal('0'), ceiling=Decimal('0.5'), group_name='groupA'),
            GroupDistribution(floor=Decimal('0.5'), ceiling=Decimal('1.0'), group_name='groupB')
        ]

        self.assertEqual(actual_group_distributions, expected_group_distributions)

    def test_validate_and_transform_distribution_with_invalid_value(self):
        input_dict = {
            "groupA": "should be a decimal",
            "groupB": Decimal(0.5)
        }
        with self.assertRaises(DistributionValueError):
            validate_and_transform_distribution(input_dict)

    def test_validate_and_transform_distribution_with_invalid_value_sum(self):
        input_dict = {
            "groupA": Decimal(0.5),
            "groupB": Decimal(0.5),
            # The decimal values should add up to 1.0
            "groupC": Decimal(0.01)
        }
        with self.assertRaises(DistributionValueSumError):
            validate_and_transform_distribution(input_dict)

    def test_assign_group_with_valid_input(self):
        group_distribution = [
            GroupDistribution(floor=Decimal('0'), ceiling=Decimal('0.5'), group_name='groupA'),
            GroupDistribution(floor=Decimal('0.5'), ceiling=Decimal('1.0'), group_name='groupB')
        ]
        self.assertEqual(assign_group(Decimal(0.0), group_distribution), "groupA")
        self.assertEqual(assign_group(Decimal(0.3), group_distribution), "groupA")
        self.assertEqual(assign_group(Decimal(0.5), group_distribution), "groupB")
        self.assertEqual(assign_group(Decimal(0.9999), group_distribution), "groupB")

    def test_assign_group_with_out_of_range_input(self):
        group_distribution = [
            GroupDistribution(floor=Decimal('0'), ceiling=Decimal('0.5'), group_name='groupA'),
            GroupDistribution(floor=Decimal('0.5'), ceiling=Decimal('1.0'), group_name='groupB')
        ]
        with self.assertRaises(ValueError):
            assign_group(Decimal(1.0), group_distribution)
        with self.assertRaises(ValueError):
            assign_group(Decimal(-0.001), group_distribution)

    def test_get_user_group_with_unassigned_user(self):
        cache = {}
        group_distributions = [
            GroupDistribution(floor=Decimal('0'), ceiling=Decimal('0.4'), group_name='groupA'),
            GroupDistribution(floor=Decimal('0.4'), ceiling=Decimal('0.5'), group_name='groupB'),
            GroupDistribution(floor=Decimal('0.5'), ceiling=Decimal('1.0'), group_name='groupC')
        ]
        # Seed random so there is deterministic output.
        random.seed(999)

        for user in self.users:
            get_user_group(user, group_distributions, cache)

        expected_user_id_group_cache = {
            "1": "groupC",
            "2": "groupA",
            "3": "groupC",
            "4": "groupC",
            "5": "groupB",
            "6": "groupA",
            "7": "groupC",
            "8": "groupA",
            "9": "groupC",
            "10": "groupC",
        }
        self.assertDictEqual(cache, expected_user_id_group_cache)

    def test_get_user_group_with_all_previously_assigned_users(self):
        cache = {
            "1": "groupC",
            "2": "groupA",
            "3": "groupC",
            "4": "groupC",
            "5": "groupB",
            "6": "groupA",
            "7": "groupC",
            "8": "groupA",
            "9": "groupC",
            "10": "groupC",
        }
        group_distributions = [
            GroupDistribution(floor=Decimal('0'), ceiling=Decimal('0.4'), group_name='groupA'),
            GroupDistribution(floor=Decimal('0.4'), ceiling=Decimal('0.5'), group_name='groupB'),
            GroupDistribution(floor=Decimal('0.5'), ceiling=Decimal('1.0'), group_name='groupC')
        ]

        # Seeding random isn't needed here.
        for user in self.users:
            get_user_group(user, group_distributions, cache)

        expected_user_id_group_cache = {
            "1": "groupC",
            "2": "groupA",
            "3": "groupC",
            "4": "groupC",
            "5": "groupB",
            "6": "groupA",
            "7": "groupC",
            "8": "groupA",
            "9": "groupC",
            "10": "groupC",
        }
        self.assertDictEqual(cache, expected_user_id_group_cache)

    def test_get_user_group_with_some_previously_assigned_users(self):
        cache = {
            "1": "groupC",
            "2": "groupA",
            "3": "groupC",
        }
        group_distributions = [
            GroupDistribution(floor=Decimal('0'), ceiling=Decimal('0.4'), group_name='groupA'),
            GroupDistribution(floor=Decimal('0.4'), ceiling=Decimal('0.5'), group_name='groupB'),
            GroupDistribution(floor=Decimal('0.5'), ceiling=Decimal('1.0'), group_name='groupC')
        ]

        # Seed random so there is deterministic output.
        random.seed(1)
        for user in self.users:
            get_user_group(user, group_distributions, cache)

        expected_user_id_group_cache = {
            "1": "groupC",
            "2": "groupA",
            "3": "groupC",
            "4": "groupA",
            "5": "groupC",
            "6": "groupC",
            "7": "groupA",
            "8": "groupB",
            "9": "groupB",
            "10": "groupC"
        }

        self.assertDictEqual(cache, expected_user_id_group_cache)
